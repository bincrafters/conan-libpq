MESSAGE("** PostgreSQL ALREADY FOUND BY CONAN!")
SET(PostgreSQL_FOUND TRUE)

find_path(PostgreSQL_INCLUDE_DIR NAMES postgres_ext.h PATHS ${CONAN_INCLUDE_DIRS_LIBPQ} NO_CMAKE_FIND_ROOT_PATH)
find_library(PostgreSQL_LIBRARY NAMES pq libpq PATHS ${CONAN_LIB_DIRS_LIBPQ} NO_CMAKE_FIND_ROOT_PATH)


list(APPEND LIBRARY_LIST ${CONAN_LIBS_LIBPQ})
if(WIN32)
    list(APPEND LIBRARY_LIST ${CONAN_LIBS_OPENSSL})
endif()

foreach(_LIBRARY_NAME ${LIBRARY_LIST})
    unset(CONAN_FOUND_LIBRARY CACHE)
    find_library(CONAN_FOUND_LIBRARY NAME ${_LIBRARY_NAME} PATHS ${CONAN_LIB_DIRS_LIBPQ} ${CONAN_LIB_DIRS_OPENSSL} NO_DEFAULT_PATH NO_CMAKE_FIND_ROOT_PATH)
    if(CONAN_FOUND_LIBRARY)
        list(APPEND PostgreSQL_LIBRARIES ${CONAN_FOUND_LIBRARY})
        message(STATUS "Found: ${CONAN_FOUND_LIBRARY}")
    else()
        message(STATUS "Library ${_LIBRARY_NAME} not found in package, might be system one")
        list(APPEND PostgreSQL_LIBRARIES ${_LIBRARY_NAME})
    endif()
endforeach()

set(PostgreSQL_INCLUDE_DIRS ${PostgreSQL_INCLUDE_DIR})
MESSAGE(STATUS "** FOUND PostgreSQL:  ${PostgreSQL_LIBRARY}")
MESSAGE(STATUS "** FOUND PostgreSQL INCLUDE:  ${PostgreSQL_INCLUDE_DIR}")
message(STATUS "PostgreSQL_LIBRARIES: ${PostgreSQL_LIBRARIES}")

mark_as_advanced(PostgreSQL_ROOT PostgreSQL_FOUND PostgreSQL_LIBRARY PostgreSQL_LIBRARIES PostgreSQL_INCLUDE_DIR)

set(PostgreSQL_MAJOR_VERSION "9")
set(PostgreSQL_MINOR_VERSION "6")
set(PostgreSQL_PATCH_VERSION "9")
